(function() {
  'use strict';

  angular
    .module('starter')
    .controller('FeedbackController', FeedbackController);

  function FeedbackController(FeedbackService, LoginService, $state) {
    console.log('initiating this');
    var vm = this;

    var questions = FeedbackService.getQuestionsFromFireBase();

    vm.answers =[];
    vm.feedbackSubmitted = false;

    questions.$loaded().then(function() {
      vm.feedbackQuestions = questions;
    });

    vm.submitAnswers = function() {
    };
  }

})();
