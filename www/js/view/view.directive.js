(function() {
  'use strict';

  angular
    .module('starter')
    .directive('view', view);

  /** @ngInject */
  function view () {
    var directive = {
      restrict: 'E',
      templateUrl: 'templates/view.html',
      scope: {},
      controller: 'ViewController',
      controllerAs: 'viewCtrl',
      bindToController: true
    };

    return directive;
  }

})();
