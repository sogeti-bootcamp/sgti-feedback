(function() {
  'use strict';

  angular
    .module('starter')
    .service('LoginService', LoginService);

  function LoginService($q) {


    var authData;

    var service = {
      getAuthData: getAuthData,
      setAuthData: setAuthData
    };

    return service;


    function getAuthData() {
      return authData;
    }

    function setAuthData(auth) {
      authData = auth;
    }

  }

})();

