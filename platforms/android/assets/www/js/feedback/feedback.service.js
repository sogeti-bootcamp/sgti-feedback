(function() {
  'use strict';

  angular
    .module('starter')
    .service('FeedbackService', FeedbackService);

  FeedbackService.$inject = ['$firebaseArray', '$q'];
  function FeedbackService($firebaseArray, $q) {
    var service = {
      submitFeedback: submitFeedback,
      getFeedback: getFeedback,
      getQuestionsFromFireBase: getQuestionsFromFirebase
    };

    return service;

    /** @ngInject */
    function submitFeedback(answers) {

      var defer = $q.defer();
      var answersFirebase = new Firebase("https://sgti-btcamp-feedback.firebaseio.com/Answers");

      return defer.promise;
    }

    function getFeedback(rows) {
      var feedbackFirebase = new Firebase("https://sgti-btcamp-feedback.firebaseio.com/Answers");

      return $firebaseArray(query);
    }

    function getQuestionsFromFirebase() {
      var questionnaire = new Firebase("https://sgti-btcamp-feedback.firebaseio.com/Questionnaire");

      return $firebaseArray(questionnaire);
    }

  }

})();
