(function() {
  'use strict';

  angular
    .module('starter')
    .directive('login', login);

  function login () {
    var directive = {
      restrict: 'E',
      templateUrl: 'templates/login.html',
      scope: {},
      controller: 'LoginController',
      controllerAs: 'loginCtrl',
      bindToController: true
    };

    return directive;
  }

})();
