(function() {
  'use strict';

  angular
    .module('starter')
    .controller('LoginController', LoginController);

  function LoginController($state, LoginService) {
    console.log('initiating this');
    var vm = this;

    vm.authData;

    var usersRef = new Firebase("https//sgti-btcamp-feedback.firebaseio.com");

    vm.login = function() {
    // remember to use state reload after authentication for the changes to reflect
    };

    vm.logout = function() {

    };

    usersRef.onAuth(function(authData) {

    });
  }

})();
